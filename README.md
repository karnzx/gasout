# Gas Out

## GasOut is a multiplayer mobile game for National Software Contest - NSC Thailand

We are from Computer Engineering, Prince of Songkla University, Thailand

- GasOut is a multiplayer game (need 4 players)
- Using the communication between players to achieve the goal
- There are many problems to solve but the description is not straight forward. One of four players will get the description and then tell the other to figure it out what to do for example: "oil is running out" - the players must use the fuel and the fuel tank to solve this problem.
  
## Preview
<img src="homepage.jpg" width="200" height="400">
<img src="lobby.jpg" width="200" height="400">

![](video-highlight.mp4)

## Setting up workspace

1. Make sure that `poetry` is installed in the system. [How to?](https://python-poetry.org/docs/)

2. Clone this repository then access it
```shell
git clone https://gitlab.com/karnzx/gasout.git
cd gasout
```

3. Due to `kivy` is not support Python 3.11 yet. If you are using Python 3.11 as default Python interpreter, make sure you have also installed Python version 3.8 - 3.10. Then set `poetry` python environment version. For example: if you want to use Python 3.10
```shell
poetry env use 3.10
```

4. Use `poetry` to install workspace's dependencies.
```shell
poetry install
```

## How to run the game

you have to setup workspace then start the server for manage data between players

### start the server before game client and 4 player must press ready then game will be start

- Starting server
```shell
./script/controller
```
or
```shell
./script/controller --topic "<INSERT YOUR TOPIC>"
```

- Starting game client
```shell
./script/run
```
or
```shell
./script/run --topic "<INSERT YOUR TOPIC>"
```

---

## Export game to **APK** for **android** by using buildozer

On 27/03/2566 buildozer image in dockerhub is not up to date so you have to build the image from github repo instead

```shell
git clone https://github.com/kivy/buildozer.git
cd buildozer
```

Replace code of `Dockerfile` _buildozer_ that you just clone by `buildozerDockerfile` code from this repo

then build the image in buildozer repo

```shell
docker build -t kivy/buildozer .
```

change directory back to this repo and run this script to build the **APK** file

```shell
./script/build-apk
```

---
