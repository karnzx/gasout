import uuid
import argparse
from kivy.config import Config

Config.set("graphics", "resizable", False)
Config.set("graphics", "width", "490")
Config.set("graphics", "height", "720")
Config.set(
    "kivy",
    "default_font",
    [
        "WareeSans",
        "app/static/fonts/WareeSans.ttf",
        "app/static/fonts/WareeSans-Bold.ttf",
    ],
)  # thai font

from kivy.app import App

try:
    from gasout.app.screens.screen import GameScreenController
    from gasout.app.mqtt.client import MQTTClient
except Exception as e:
    print("error:", e)
    import app
    from app.screens.screen import GameScreenController
    from app.mqtt.client import MQTTClient


class GasOutApp(App):
    def __init__(self, topic, **kwargs):
        super().__init__(**kwargs)
        self.unique_id = str(uuid.uuid4())
        self.topic = topic
        self.lobby = None
        self.client = MQTTClient(topic=topic)

        print("unique_id", self.unique_id)

    def build(self):
        return GameScreenController(client=self.client)

    def on_start(self):
        self.client.loop_start()
        self.client.send_message("gasout.join_lobby", True)

    def on_stop(self):
        self.client.send_message("gasout.leave", True, player=self.unique_id)
        self.client.loop_stop()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--topic", help="topic of communication client")
    if not (topic := parser.parse_args().topic):
        topic = "TEST-MQTT"
    print(topic)
    GasOutApp(topic=topic).run()
