import kivy

from kivy.uix.screenmanager import Screen
from kivy.lang import Builder

Builder.load_file("app/layout/gamescreen.kv")


class GameScreen(Screen):
    pass