import kivy

from kivy.lang.builder import Builder
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.uix.image import Image
from kivy.properties import ObjectProperty, StringProperty, ListProperty
from kivy.uix.screenmanager import Screen
from kivy.uix.behaviors import ButtonBehavior
from ...game_controller import AnimateBookImage, AnimateZombiesImage

Builder.load_file("app/layout/manual_book/manual_3.kv")


class Manual_3(Screen):

    def __init__(self, **kwargs):
        super(Manual_3, self).__init__(**kwargs)

    def change_screen(self, screen_name):
        self.manager.current = screen_name
