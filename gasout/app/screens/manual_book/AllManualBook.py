import kivy

from kivy.lang.builder import Builder
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.uix.image import Image
from kivy.properties import ObjectProperty, StringProperty, ListProperty
from kivy.uix.screenmanager import Screen
from kivy.uix.behaviors import ButtonBehavior
from ...game_controller import AnimateBookImage, AnimateZombiesImage
from kivy.properties import NumericProperty

Builder.load_file("app/layout/manual_book/all_manual_book.kv")


class AllManualBook(Screen):
    page_number = NumericProperty(1)
    def __init__(self, **kwargs):
        super(AllManualBook, self).__init__(**kwargs)

    def change_screen(self, screen_name):
        self.manager.current = screen_name

    def go_page_left(self):
        if self.page_number > 1:
            self.page_number -= 1
        
    def go_page_right(self):
        if self.page_number < 6:
            self.page_number += 1
        