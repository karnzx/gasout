import kivy, time
from kivy.app import App
from kivy.uix.image import Image
from kivy.uix.screenmanager import Screen
from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.clock import Clock, mainthread
from kivy.properties import StringProperty
from kivy.uix.behaviors import ToggleButtonBehavior

try:
    from gasout.app.mqtt.client import MQTTClient
except Exception as e:
    print("error", e)
    from app.mqtt.client import MQTTClient

Builder.load_file("app/layout/lobbyscreen.kv")


class AnimationImage(ToggleButtonBehavior, Image):
    client = ObjectProperty(None)
    time = 0.0
    rate = 0.2
    frame = 1

    def __init__(self, **kwargs):
        super(AnimationImage, self).__init__(**kwargs)
        self.app = App.get_running_app()
        self.picture = "atlas://app/static/images/lobby/unreadybutton/Unready_button-"

    def update(self, dt):
        self.time += dt
        if self.time > self.rate:
            self.time -= self.rate
            self.source = self.picture + str(self.frame)
            # print(self.ids.button.source)
            self.frame = self.frame + 1
            if self.frame > 4:
                self.frame = 1
            # if (
            #     self.picture
            #     == "atlas://app/static/images/lobby/ready_button/ready_button-"
            # ):
            #     time.sleep(1)

    def on_state(self, widget, value):
        print(value)

        if value == "down":
            self.picture = "atlas://app/static/images/lobby/ready_button/ready_button-"
            self.client.send_message("gasout.ready", "1", player=self.app.unique_id)
        else:
            self.picture = (
                "atlas://app/static/images/lobby/unreadybutton/Unready_button-"
            )
            self.client.send_message("gasout.ready", "0", player=self.app.unique_id)


import threading


class LobbyScreen(Screen):
    status = 0

    def __init__(self, client: MQTTClient, **kwargs):
        self.player_screen = {
            "gasout.first_player": "FirstPlayer",
            "gasout.second_player": "SecondPlayer",
            "gasout.third_player": "ThirdPlayer",
            "gasout.fourth_player": "FourthPlayer",
        }
        self.app = App.get_running_app()
        self.client = client
        super(LobbyScreen, self).__init__(**kwargs)

        if self.status == 0:
            self.clock = Clock.schedule_interval(
                self.ids.animationbutton.update, 1.0 / 60
            )

    def on_enter(self):
        self.client.set_new_callback(self.app.unique_id, self.on_message_callback)
        self.client.set_new_callback("gasout.lobby", self.on_message_lobby)
        self.client.send_message("gasout.join_lobby", True, player=self.app.unique_id)

    @mainthread
    def change_screen(self, screen_name):
        print("screen", "==", screen_name)
        self.clock.cancel()  # stop the clock for running in button
        self.manager.current = screen_name

    def leave(self):
        self.client.send_message("gasout.leave", True, player=self.app.unique_id)
        self.change_screen("Homepage")

    def on_message_callback(self, role, **kwargs):
        print(role, kwargs)
        self.change_screen(self.player_screen[role])

    def on_message_lobby(self, join, ready, number, **kwargs):
        print(kwargs)
        self.ids.player_quantity.text = (
            f"{join} players joined\n{ready} player is ready"
        )
        self.ids.room_number.text = f"Room: {number}"
