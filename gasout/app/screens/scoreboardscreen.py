from kivy.uix.screenmanager import Screen
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.graphics import Color, Line
import random
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.properties import DictProperty
from kivy.lang import Builder
from kivy.clock import mainthread
from kivy.metrics import Metrics
from kivy.core.window import Window
        
Builder.load_file("app/layout/scoreboard.kv")

class ScoreBoardScreen(Screen):
    def __init__(self, client, **kwargs):
        super(ScoreBoardScreen, self).__init__(**kwargs)
        self.client = client
        self.visited = False
        self.layout = None
        self.player_score = {}
    
    def on_pre_enter(self):
        self.client.set_new_callback('gasout.scoreboard', self.on_scoreboard)
        self.client.send_message('gasout.score', True)
        

    def on_enter(self):
        
        if self.visited:
            self.remove_widget(self.layout)
        else:
            self.visited = True
        
        self.lines_x = None
        self.lines_y = []
        self.rows = len(self.player_score)
        
        title_label = Label(text="Scoreboard", font_size=Metrics.sp*30, bold=True, size_hint=(0.5, 0.1), pos_hint={'center_x': 0.5, 'top': 1})
        self.add_widget(title_label)
        # Create the layout
        layout = GridLayout(cols=2, rows=self.rows+1, padding=(20, 10), spacing=(20, 10), size_hint=(None, None), size=(400, 500))
        self.layout = layout
        # Add the header row
        header_name = Label(text="ชื่อผู้เล่น", size_hint=(0.5, 0.3), halign='left', bold=True, color=(1,0,1,1))
        header_score = Label(text="คะแนน", size_hint=(0.5, 0.3), halign='left', bold=True, color=(1,0,1,1))
        layout.add_widget(header_name)
        layout.add_widget(header_score)
        
        sorted_list = sorted(self.player_score, key=lambda item: item[1], reverse=True)
        for index, value in enumerate(sorted_list):
            name = value[0]
            score = value[1]
            label_name = Label(text=f"{index+1}. {name}", size_hint=(0.5, 0.3), halign='left')
            label_score = Label(text=f"{score}", size_hint=(0.5, 0.3), halign='left')
            layout.add_widget(label_name)
            layout.add_widget(label_score)

        # Set the pos_hint to center the layout
        layout.pos_hint = {'center_x': 0.5, 'center_y': 0.5}
        
        # Add the layout to the screen
        self.add_widget(layout)

    @mainthread
    def on_scoreboard(self, scores, **kwargs):
        self.player_score = scores
        self.on_enter()
