import kivy
import time

from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.uix.screenmanager import Screen


Builder.load_file("app/layout/endscreen.kv")


class EndScreen(Screen):
    pass


class TextLabel(Widget):
    pass
