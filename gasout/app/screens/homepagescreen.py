import kivy

from kivy.uix.screenmanager import Screen
from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.image import Image
from kivy.core.window import Window
from ..game_controller import AnimateZombiesImage
from kivy.clock import Clock

Builder.load_file("app/layout/homepagescreen.kv")


class HomepageScreen(Screen):
    def __init__(self, client, **kwargs):
        super().__init__(**kwargs)
        self.client = client
        self.clock = Clock.schedule_interval(
            self.ids.background.scroll_textures, 1 / 60
        )
        self.clock_truck = Clock.schedule_interval(self.ids.truckatlas.update, 1 / 60)
        self.clock_zombies = Clock.schedule_interval(
            self.ids.homezombies.update, 1 / 60
        )

    def on_enter(self):
        self.clock()
        self.client.reset()

    def change_screen(self, screen_name):
        self.clock.cancel()  # stop the clock for running in bg
        self.manager.current = screen_name


class BackGround(Widget):
    town_source = StringProperty("")
    town_texture = ObjectProperty(None)
    cloud_texture = ObjectProperty(None)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.town_texture = None
        self.cloud_texture = Image(source="app/static/images/home/cloud.png").texture

        self.cloud_texture.wrap = "repeat"
        self.cloud_texture.uvsize = (Window.width / self.cloud_texture.width, -1)

    def setup(self):
        self.town_texture = Image(source=self.town_source).texture
        self.town_texture.wrap = "repeat"
        self.town_texture.uvsize = (0.1, -1)

    def scroll_textures(self, time_passed):
        # update uvpos
        if not self.town_texture:
            self.setup()

        self.town_texture.uvpos = (
            (self.town_texture.uvpos[0] + 0.0007),
            self.town_texture.uvpos[1],
        )
        self.cloud_texture.uvpos = (
            (self.cloud_texture.uvpos[0] + 0.006),
            self.cloud_texture.uvpos[1],
        )
        # redraw
        texture = self.property("town_texture")
        texture.dispatch(self)

        texture = self.property("cloud_texture")
        texture.dispatch(self)


class AnimationTruck(Image):
    time = 0.0
    rate = 0.3
    frame = 1

    def update(self, dt):
        # making truck move
        self.time += dt
        if self.time > self.rate:
            self.time -= self.rate
            self.source = (
                "atlas://app/static/images/lobby/truckatlas/special_truck-"
                + str(self.frame)
            )
            # print(self.ids.truckatlas.source)
            self.frame = self.frame + 1
            if self.frame > 2:
                self.frame = 1


class SettingLabel(Widget):
    pass