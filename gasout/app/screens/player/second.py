import kivy

from kivy.lang.builder import Builder
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.uix.image import Image
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.screenmanager import Screen
from .player import Player
from kivy.uix.behaviors import ButtonBehavior
from ...game_controller import MagicWidget

Builder.load_file("app/layout/player/second.kv")


class SecondPlayerScreen(Player):
    def __init__(self, **kwargs):
        self.client = kwargs.pop("client")
        super().__init__(**kwargs)
        self.clock_zombies = Clock.schedule_interval(
            self.ids.secondzombies.update, 1 / 60
        )

    def on_pre_enter(self):
        self.client.set_new_callback(
            "gasout.second_player", self.on_message_second_player
        )

    def send_message(self, text):
        # {"topic": "xxx", "kwargs": {"msg": "message"}}
        self.client.send_message("gasout.second_player", text, second_msg="hi again")
        print("sended")

    def on_message_second_player(self, **kwargs):
        print("gasout.second_player", kwargs)
        if kwargs.get("msg") == "quest":
            self.got_quest = True
            self.content = kwargs.get("content")
        elif kwargs.get("msg") == "gear":
            self.ids.gear.set_gear(kwargs.get("No."))


class GearChanger(ButtonBehavior, Image):
    client = ObjectProperty(None)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.source = "atlas://app/static/images/secondplayer/gear/gear-5"
        self.frame = int(self.source[-1])

    def set_gear(self, number):
        self.frame = number
        self.source = self.source[:-1] + str(number)

    def on_press(self):
        self.frame += 1
        if self.frame > 5:
            self.frame = 1
        self.source = self.source[:-1] + str(self.frame)
        self.client.send_message(
            "gasout.solve", f"gear-{self.frame}", player="gasout.second_player"
        )
        print("On gear: " + str(self.frame))
