import kivy

from kivy.lang.builder import Builder
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.uix.image import Image
from kivy.properties import ObjectProperty, StringProperty, ListProperty
from kivy.uix.screenmanager import Screen
from kivy.uix.behaviors import ButtonBehavior
from .player import Player
from ...game_controller import AnimateBookImage, AnimateZombiesImage

Builder.load_file("app/layout/player/fourth.kv")


class FourthPlayerScreen(Player):
    def __init__(self, **kwargs):
        self.client = kwargs.pop("client")
        super().__init__(**kwargs)
        self.clock_zombies = Clock.schedule_interval(
            self.ids.fourthzombies.update, 1 / 60
        )

    def on_pre_enter(self):
        self.client.set_new_callback(
            "gasout.fourth_player", self.on_message_fourth_player
        )

    def send_message(self, text):
        # {"topic": "xxx", "kwargs": {"msg": "message"}}
        self.client.send_message("gasout.fourth_player", text, fourth_msg="hi again")
        print("sended")

    def on_message_fourth_player(self, **kwargs):
        print("gasout.fourth_player", kwargs)
        if kwargs.get("msg") == "quest":
            self.got_quest = True
            self.content = kwargs.get("content")
