import kivy

from kivy.lang.builder import Builder
from kivy.uix.widget import Widget
from kivy.clock import Clock, mainthread
from kivy.uix.image import Image
from kivy.properties import ObjectProperty, StringProperty, NumericProperty
from kivy.uix.screenmanager import Screen
from ...game_controller import AnimateBookImage, AnimateZombiesImage, MagicWidget
from kivy.factory import Factory
from kivy.core.audio import SoundLoader
import threading
from ..homepagescreen import BackGround, AnimationTruck

Factory.register("MagicWidget", cls=MagicWidget)
Builder.load_file("app/layout/player/magicbutton.kv")


class Player(Screen):
    client = ObjectProperty(None)
    content = StringProperty("")
    score = NumericProperty(0)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.ids.pb.max = self.ids.pb.value = 20
        self.visited = False
        self.got_quest = False

    def on_enter(self):
        if self.visited:
            return
        self.bg_clock = Clock.schedule_interval(
            self.ids.background.scroll_textures, 1 / 60
        )
        self.truck_clock = Clock.schedule_interval(self.ids.truckatlas.update, 1 / 60)
        self.pb_clock = Clock.schedule_interval(self.update_pb, 1 / 60)
        self.pb_clock.cancel()
        self.client.set_new_callback("gasout.truck", self.on_message_truck)
        self.client.set_new_callback("gasout.all", self.on_message_all)
        self.client.set_new_callback("gasout.score", self.on_message_score)
        self.client.set_new_callback("gasout.forceclose", self.on_lobby_close)
        self.visited = True
        self.background_song = SoundLoader.load("gasout/app/static/audio/loonboon.wav")
        self.countdown_sound = SoundLoader.load("gasout/app/static/audio/countdown_8bit.wav")
        Clock.schedule_once(lambda dt: threading.Thread(target=self.play_background_song).start())
    
    def on_message_score(self, **kwargs):
        sc = kwargs.get("msg")
        print("score", sc)
        self.score = sc
        
    def play_background_song(self):
        if self.background_song:
            self.background_song.loop = True
            self.background_song.volume = 0.5
            self.background_song.play()
            
            
    def play_countdown_sound(self):
        if self.countdown_sound:
            self.countdown_sound.volume = 0.5
            self.countdown_sound.play()
        
    def on_message_all(self, **kwargs):
        print(kwargs.get("msg"))
        if kwargs.get("msg") == "timer":
            self.set_pb_max(kwargs.get("time"))
            self.pb_clock()
        elif kwargs.get("msg") == "stop_timer":
            self.content = ""
            self.stop_pb()
        elif kwargs.get("msg") == "wrong":
            self.decrease_pb(kwargs.get("decrease_time"))
        else:
            self.content = kwargs.get("msg")
            if kwargs.get("msg") in ['3','2','1','เริ่ม!!']:
                Clock.schedule_once(lambda dt: self.play_countdown_sound(), 0.3)
            elif "END" in kwargs.get("msg"):
                self.client.reset_topic()

    def on_message_truck(self, **kwargs):
        print("truck", kwargs)
        if kwargs.get("msg") == "decrease":
            self.backward_truck()
            self.content = "OH NO!"
        elif kwargs.get("msg") == "increase":
            self.forward_truck()
            self.content = "GOOD!"

    def update_pb(self, dt):
        if self.ids.pb.value <= 0:
            if self.got_quest:
                self.client.send_message("gasout.truck", "decrease")
                self.got_quest = False
            self.stop_pb()
        self.ids.pb.value -= dt

    def set_pb_max(self, value):
        if value:
            self.ids.pb.max = self.ids.pb.value = value

    def decrease_pb(self, decrease_time):
        if self.ids.pb.value != self.ids.pb.max:  # if it not running dont decrease time
            self.ids.pb.value -= decrease_time

    def reset_pb(self):
        self.ids.pb.value = self.ids.pb.max

    def stop_pb(self):
        self.pb_clock.cancel()
        # delay because reset fast than pb_clock.cancel()
        Clock.schedule_once(lambda dt: self.reset_pb(), 0.1)

    @mainthread
    def change_screen(self, screen_name):
        self.manager.current = screen_name
        
    def forward_truck(self):
        self.content = ""
        if self.ids.truckatlas.pos_hint["center_x"] < 0.634:
            self.ids.truckatlas.pos_hint = {
                "center_x": self.ids.truckatlas.pos_hint["center_x"] + 0.067
            }
        else:
            print(self.ids.truckatlas.pos_hint["center_x"])
            print("WELL DONE")

    def backward_truck(self):
        self.content = ""
        if self.ids.truckatlas.pos_hint["center_x"] > 0.3:
            self.ids.truckatlas.pos_hint = {
                "center_x": self.ids.truckatlas.pos_hint["center_x"] - 0.067
            }
        else:
            self.client.send_message("gasout.gameover", True)
            self.client.reset_topic()
            print("Game Over")
            self.change_screen("Endscreen")
    
    def on_lobby_close(self, **kwargs):
        self.client.reset_topic()
        print("SERVER CLOSE")
        self.change_screen("Endscreen")
