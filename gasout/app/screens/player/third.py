import kivy

from kivy.lang.builder import Builder
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.uix.image import Image
from kivy.properties import ObjectProperty, StringProperty, ListProperty
from kivy.uix.screenmanager import Screen
from kivy.uix.behaviors import ButtonBehavior
from .player import Player
from ...game_controller import AnimateBookImage, AnimateZombiesImage

Builder.load_file("app/layout/player/third.kv")


class ThirdPlayerScreen(Player):
    def __init__(self, **kwargs):
        self.client = kwargs.pop("client")
        super().__init__(**kwargs)
        self.clock_zombies = Clock.schedule_interval(
            self.ids.thirdzombies.update, 1 / 60
        )
        self.clock_engine = Clock.schedule_interval(self.ids.engine.update, 1 / 60)

    def on_pre_enter(self):
        self.client.set_new_callback(
            "gasout.third_player", self.on_message_third_player
        )

    def send_message(self, text):
        # {"topic": "xxx", "kwargs": {"msg": "message"}}
        self.client.send_message("gasout.third_player", text, third_msg="hi again")
        print("sended")

    def on_message_third_player(self, **kwargs):
        print("gasout.third_player", kwargs)
        if kwargs.get("msg") == "quest":
            self.got_quest = True
            self.content = kwargs.get("content")
        elif kwargs.get("msg") == "engine_heat":
            self.change_engine_image(kwargs.get("status"))
        elif kwargs.get("msg") == "engine_stop":
            self.stop_engine_image(kwargs.get("status"))
        elif kwargs.get("msg") == "switch":
            self.ids.switch.change_state(kwargs.get("state"))

    def change_engine_image(self, status):
        # print("gasout.third_player", kwargs)
        if status == "heat":
            self.ids.engine.engine_source = (
                "atlas://app/static/images/thirdplayer/hot_engine/Engine-1"
            )
        elif status == "normal":
            self.ids.engine.engine_source = (
                "atlas://app/static/images/thirdplayer/engine/Engine-1"
            )

    def stop_engine_image(self, status):
        # print("gasout.third_player", kwargs)
        if status == "stop":
            print("stop")
            self.clock_engine.cancel()
        elif status == "normal":
            print("normal")
            self.clock_engine()


class ButtonChanger(ButtonBehavior, Image):
    client = ObjectProperty(None)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.frame = 1

    def change_state(self, state):
        self.frame = 3 if state == "on" else 2
        self.source = self.source[:-1] + str(self.frame)

    def on_press(self):  # 2 off, 3 on
        self.frame += 1
        tmp = self.frame
        if self.frame > 3:
            self.frame = 2
        self.source = self.source[:-1] + str(self.frame)
        print("On state : " + str(self.frame))
        self.client.send_message(
            "gasout.solve",
            f"switch-{'on' if self.frame == 3 else 'off'}",
            player="gasout.third_player",
        )


class AnimateEngineImage(Image):
    time = 0.0
    rate = 0.1
    frame = 1
    engine_source = StringProperty("")

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.source = self.engine_source

    def update(self, dt):
        # making engine move;
        self.time += dt
        if self.time > self.rate:
            self.time -= self.rate
            self.source = self.engine_source[:-1] + str(self.frame)
            # print(self.ids.truckatlas.source)
            self.frame = self.frame + 1
            if self.frame > 4:
                self.frame = 1
