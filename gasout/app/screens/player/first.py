import kivy

from kivy.lang.builder import Builder
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.uix.image import Image
from kivy.properties import ObjectProperty, StringProperty, ListProperty
from kivy.uix.screenmanager import Screen
from kivy.uix.behaviors import ButtonBehavior
from .player import Player
from ...game_controller import AnimateBookImage, AnimateZombiesImage

from plyer import gravity

Builder.load_file("app/layout/player/first.kv")


class FirstPlayerScreen(Player):
    def __init__(self, **kwargs):
        self.client = kwargs.pop("client")
        super().__init__(**kwargs)
        self.clock_zombies = Clock.schedule_interval(
            self.ids.firstplayerzombie.update, 1 / 60
        )
        self.gas_gauge_clock = None
        self.frame = None
        self.sensorEnabled = False
        self.gravity_clock = None

    def on_pre_enter(self):
        self.client.set_new_callback(
            "gasout.first_player", self.on_message_first_player
        )
        try:
            if not self.sensorEnabled:
                gravity.enable()
                self.gravity_clock = Clock.schedule_interval(self.get_gravity, 1)
                self.gravity_clock.cancel()
                self.sensorEnabled = True
        except NotImplementedError:
            import traceback

            traceback.print_exc()

    def get_gravity(self, dt):
        val = gravity.gravity

        if not val == (None, None, None):
            self.x = val[0]
            self.y = val[1]
            self.z = val[2]
            Clock.schedule_once(self.is_shake, 1.3)

    def is_shake(self, dt):
        val = gravity.gravity
        if not val == (None, None, None):
            x = val[0]
            y = val[1]
            z = val[2]
            if abs(self.x - x) > 1 or abs(self.y - y) > 1 or abs(self.z - z) > 1:
                self.client.send_message(
                    "gasout.solve", "lever-on", player="gasout.first_player"
                )

    def send_message(self, text):
        # {"topic": "xxx", "kwargs": {"msg": "message"}}
        self.client.send_message("gasout.first_player", text, second_msg="hi again")
        print("sended")

    def on_message_first_player(self, **kwargs):
        print("gasout.first_player", kwargs)
        if kwargs.get("msg") == "quest":
            self.got_quest = True
            self.content = kwargs.get("content")
        elif kwargs.get("msg") == "gas_gauge":
            if self.gas_gauge_clock:
                self.gas_gauge_clock.cancel()
            if kwargs.get("status") == "decrease":
                self.time = 0
                self.rate = 0.3
                if not self.frame:
                    self.frame = 2
                self.frame_end = 18
                self.frame_direction = 1
            elif kwargs.get("status") == "increase":
                self.time = 0
                self.rate = 0.3
                self.frame_end = 2
                self.frame_direction = -1
            self.gas_gauge_clock = Clock.schedule_interval(self.animate_gas_gauge, 1)
        elif kwargs.get("msg") == "shake":
            if kwargs.get("state") == "start":
                self.gravity_clock()
            elif kwargs.get("state") == "stop":
                self.gravity_clock.cancel()

    def animate_gas_gauge(self, dt):
        self.time += dt
        print("frame   ", self.frame)
        if self.time > self.rate:
            self.time -= self.rate
            self.ids.gas_gauge.source = (
                self.ids.gas_gauge.source.rsplit("-")[0] + "-" + str(self.frame)
            )

            self.frame = self.frame + self.frame_direction
            if self.frame_direction == 1:
                if self.frame > self.frame_end:
                    self.gas_gauge_clock.cancel()
            else:
                if self.frame < self.frame_end:
                    self.gas_gauge_clock.cancel()


class NumpadButton(ButtonBehavior, Image):
    display = ListProperty(None)
    client = ObjectProperty(None)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.allow_stretch = True
        self.keep_ratio = False

    def on_press(self):
        for i in self.display:
            if not i.text:
                i.text = self.source[-1]
                break
            # print(i.text)
        if self.display[3].text:
            self.client.send_message(
                "gasout.solve",
                self.display[0].text
                + self.display[1].text
                + self.display[2].text
                + self.display[3].text,
                player="gasout.first_player",
            )
            Clock.schedule_once(self.clear_output, 0.1)
        # print("pressed", self.source[-1])

    def clear_output(self, dt):
        # clear
        for i in self.display:
            i.text = ""


class NumpadDelete(ButtonBehavior, Image):
    display = ListProperty(None)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.allow_stretch = True
        self.keep_ratio = False

    def on_press(self):
        for i in range(3, -1, -1):
            if self.display[i].text:
                self.display[i].text = ""
                break
        # print("pressed delete")
