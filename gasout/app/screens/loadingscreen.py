import kivy

from kivy.uix.screenmanager import Screen
from kivy.lang import Builder

Builder.load_file("app/layout/loadingscreen.kv")


class LoadingScreen(Screen):
    pass