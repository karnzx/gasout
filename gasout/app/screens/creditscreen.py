import kivy

from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.uix.screenmanager import Screen


Builder.load_file("app/layout/creditscreen.kv")


class CreditScreen(Screen):
    pass


class TextLabel(Widget):
    pass
