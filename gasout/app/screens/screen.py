import kivy

from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
from .homepagescreen import HomepageScreen
from .settingscreen import SettingScreen
from .creditscreen import CreditScreen
from .lobbyscreen import LobbyScreen
from .loadingscreen import LoadingScreen
from .endscreen import EndScreen
from .gamescreen import GameScreen
from kivy.properties import StringProperty
from .joinroompage import JoinRoomScreen
from .player import FirstPlayerScreen, SecondPlayerScreen, ThirdPlayerScreen, FourthPlayerScreen
from .manual_book.AllManualBook import AllManualBook
from .manual_book.Manual_1 import Manual_1
from .manual_book.Manual_1_Page_2 import Manual_1_Page_2
from .manual_book.Manual_2 import Manual_2
from .manual_book.Manual_3 import Manual_3
from .manual_book.Manual_4_Page_1 import Manual_4_Page_1
from .manual_book.Manual_4_Page_2 import Manual_4_Page_2
from ..mqtt.client import MQTTClient
from .scoreboardscreen import ScoreBoardScreen
from .tutorial.all_tutorial import Tutorial


class GameScreenController(ScreenManager):

    def __init__(self, client):
        super().__init__(transition=NoTransition())
        self.client = client
        self.buildscreen()

    def buildscreen(self):
        self.add_widget(HomepageScreen(name="Homepage", client=self.client))
        self.add_widget(SettingScreen(name="Setting"))
        self.add_widget(CreditScreen(name="Credit"))
        self.add_widget(LobbyScreen(name="Lobby", client=self.client))
        self.add_widget(LoadingScreen(name="Loading"))
        self.add_widget(GameScreen(name="Game"))
        self.add_widget(JoinRoomScreen(name="JoinRoom", client=self.client))
        self.add_widget(FirstPlayerScreen(name="FirstPlayer", client=self.client))
        self.add_widget(SecondPlayerScreen(name="SecondPlayer", client=self.client))
        self.add_widget(ThirdPlayerScreen(name="ThirdPlayer", client=self.client))
        self.add_widget(FourthPlayerScreen(name="FourthPlayer", client=self.client))
        self.add_widget(Manual_1(name="FirstManualPage1"))
        self.add_widget(Manual_1_Page_2(name="FirstManualPage2"))
        self.add_widget(Manual_2(name="SecondManual"))
        self.add_widget(Manual_3(name="ThirdManualPage1"))
        self.add_widget(Manual_4_Page_1(name="FourthManualPage1"))
        self.add_widget(Manual_4_Page_2(name="FourthManualPage2"))
        self.add_widget(EndScreen(name="Endscreen"))
        self.add_widget(AllManualBook(name="AllManualBook"))
        self.add_widget(Tutorial(name="Tutorial"))
        self.add_widget(ScoreBoardScreen(name='ScoreBoardScreen', client=self.client))
