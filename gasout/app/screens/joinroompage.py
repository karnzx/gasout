import kivy
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.uix.screenmanager import Screen
from kivy.uix.textinput import TextInput
from kivy.clock import Clock, mainthread
from string import punctuation

Builder.load_file("app/layout/joinroom.kv")

try:
    from gasout.app.mqtt.client import MQTTClient
except Exception as e:
    print("error:", e)
    from app.mqtt.client import MQTTClient


class JoinRoomScreen(Screen):
    def __init__(self, client: MQTTClient, **kwargs):
        super().__init__(**kwargs)
        self.app = App.get_running_app()
        self.client = client
        self.number_input = LobbyNumberTextInput()
        self.add_widget(self.number_input)
        self.nameInput = LobbyNameTextInput()
        self.add_widget(self.nameInput)

    def on_pre_enter(self):
        self.client.set_new_callback(self.app.unique_id, self.on_message_callback)

    def on_message_callback(self, number, **kwargs):
        self.client.change_topic(topic=f"{self.client.topic}/lobby/{number}")
        print(self.client.topic)
        self.client.remove_callback(self.app.unique_id)
        self.change_screen("Lobby")

    @mainthread
    def change_screen(self, screen_name):
        print("scren == ", screen_name)
        self.manager.current = screen_name

    def host(self):
        room_name = str(self.nameInput.text)
        if room_name == "":
            return
        print("sent", "host", self.app.unique_id, "room", room_name)
        self.client.send_message(
            "gasout.host", True, player=self.app.unique_id, room_name=room_name
        )

    def join(self):
        number = str(self.number_input.text)
        if not number.isdigit():
            return
        print(int(number))
        self.client.send_message(
            "gasout.join", True, player=self.app.unique_id, number=number
        )


# Name for scoring
class LobbyNameTextInput(TextInput):
    def __init__(self, **kwargs):
        super().__init__(
            size_hint=(0.6, 0.05),
            pos_hint={"center_x": 0.5, "center_y": 0.7},
            halign="center",
            multiline=False,
            **kwargs,
        )
        self.hint_text = "ชื่อห้อง"
        self.bind(text=self.on_text)

    def on_text(self, instance, value):
        value = str(value)
        if value == "":
            return
        if value[-1] in punctuation:
            self.text = value[:-1]
        if len(value) > 10:
            self.text = value[:10]


class LobbyNumberTextInput(TextInput):
    def __init__(self, **kwargs):
        super().__init__(
            size_hint=(0.6, 0.05),
            pos_hint={"center_x": 0.5, "center_y": 0.4},
            halign="center",
            multiline=False,
            **kwargs,
        )
        self.hint_text = "เลขห้อง"
        self.bind(text=self.on_text)

    def on_text(self, instance, value):
        value = str(value)
        if value == "":
            return
        if value[-1] not in "0123456789":
            self.text = value[:-1]
        if len(value) > 4:
            self.text = value[:4]
