import paho.mqtt.client as mqtt
import json


class MQTTClient(mqtt.Client):

    def __init__(self, topic):
        super().__init__()
        self.connect(host='broker.mqttdashboard.com', port=1883)
        self.on_connect = self.on_connect_callback
        self.on_disconnect = self.on_disconnect_callback
        self.on_message = self.on_message_callback
        self.callbacks = dict()
        self.topic = topic
        self.__topic = topic

    def on_connect_callback(self, client, userdata, flags, rc):
        print("Connected with result code", rc)
        self.subscribe(self.topic)
    
    def on_disconnect_callback(self, client, userdata, rc):
        print('Disconnect with code:', str(rc))
        self.unsubscribe(self.topic)

    def on_message_callback(self, client, userdata, msg):
        """The callback for when a PUBLISH message is received from the server."""
        # print(msg.topic + " " + str(msg.payload.decode("utf-8", "strict")))
        try:
            data = json.loads(msg.payload.decode("utf-8", "strict"))
            if data == 1:  # ignore '1' (unknown msg that come when start the game)
                return
        except Exception as e:
            print("error json loading -> ", e, "data->", msg.payload)
            data = {}
        callback = self.callbacks.get(data.get("topic"))
        if callback:
            callback(**data["kwargs"])

    def send_message(self, topic, text, **kwargs):
        message = json.dumps({"topic": topic, "kwargs": {"msg": text, **kwargs}})
        self.publish(self.topic, message)

    def set_new_callback(self, topic, callback):
        """set callback to topic"""
        self.callbacks[topic] = callback
    
    def remove_callback(self, *args):
        for topic in args:
            self.callbacks.pop(topic, None)
    
    def reset(self, dont_reset_topic=False):
        del self.callbacks
        self.callbacks = dict()
        if not dont_reset_topic:
            self.reset_topic()
    
    def reset_topic(self):
        self.change_topic(self.__topic)

    def change_topic(self, topic):
        self.unsubscribe(self.topic)
        self.subscribe(topic)
        self.topic = topic
