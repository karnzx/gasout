from kivy.app import App
from kivy.lang import Builder
from kivy.animation import Animation
from kivy.factory import Factory
from random import gauss


class MagicWidget(object):
    def grow(self):
        Animation.stop_all(self)
        (
            Animation(scale_x=1.2, scale_y=1.2, t="out_quad", d=0.03)
            + Animation(scale_x=1, scale_y=1, t="out_elastic", d=0.4)
        ).start(self)

    def shake(self):
        Animation.stop_all(self)
        (
            Animation(translate_x=50, t="out_quad", d=0.02)
            + Animation(translate_x=0, t="out_elastic", d=0.5)
        ).start(self)

    def wobble(self):
        Animation.stop_all(self)
        (
            (
                Animation(scale_y=0.7, t="out_quad", d=0.03)
                & Animation(scale_x=1.4, t="out_quad", d=0.03)
            )
            + (
                Animation(scale_y=1, t="out_elastic", d=0.5)
                & Animation(scale_x=1, t="out_elastic", d=0.4)
            )
        ).start(self)

    def twist(self):
        Animation.stop_all(self)
        (
            Animation(rotate=25, t="out_quad", d=0.05)
            + Animation(rotate=0, t="out_elastic", d=0.5)
        ).start(self)

    def shrink(self):
        Animation.stop_all(self)
        Animation(scale_x=0.95, scale_y=0.95, t="out_quad", d=0.1).start(self)
