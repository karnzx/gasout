from kivy.uix.image import Image
from kivy.properties import StringProperty, NumericProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.clock import Clock


class AnimateBookImage(ButtonBehavior, Image):
    frame = NumericProperty(1)
    frame_end = NumericProperty(2)
    rate = NumericProperty(0.3)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.time = 0.0
        self.clock = None

    # suggest to call by clock see Example at firstplayer book
    def start_animate(self):
        if not self.clock:
            self.clock = Clock.schedule_interval(self.animate_once, 1 / 60)

    def animate_once(self, dt):
        self.time += dt
        if self.time > self.rate:
            self.time -= self.rate
            self.source = self.source[:-1] + str(self.frame)
            self.frame = self.frame + 1
            if self.frame > self.frame_end:
                self.frame = 1
                self.source = self.source[:-1] + str(self.frame)
                if self.clock:
                    self.clock.cancel()
                    self.clock = None
