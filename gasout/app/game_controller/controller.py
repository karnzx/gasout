import threading
import time
import random
import uuid
import datetime
from gasout.app.mqtt.client import MQTTClient
from gasout.app.game_controller.problem import problems
from gasout.app.game_controller import utils
from gasout.app.game_controller import utils


class GasOutController(threading.Thread):
    def __init__(self, topic, name, number):
        super().__init__()
        self.number = number
        self.name = name
        self.topic = topic
        self.client = MQTTClient(topic=f'{topic}/lobby/{number}')
        self.client.set_new_callback('gasout.join_lobby', self.on_player_join)
        self.client.set_new_callback('gasout.ready', self.on_player_ready)
        self.client.set_new_callback('gasout.leave', self.on_player_leave)
        self.client.set_new_callback("gasout.solve", self.on_message_solve)
        self.client.set_new_callback("gasout.truck", self.on_message_truck)
        self.client.set_new_callback("gasout.gameover", self.on_message_gameover)
        self.client.loop_start()
        # Game
        self.game_running = False
        self.solved = True
        self.players = dict()
        self.solve_value = None
        self.score = 0
    
    def on_player_join(self, player, **kwargs):
        print('[JOINED]', player)
        if self.game_running:
            print('[JOINED]', 'ERR: the game is running')
            return
        if player in self.players:
            print('[JOINED]', 'ERR: existed player')
            return
        if len(self.players) >= 4:
            print('[JOINED]', 'ERR: full')
            return
        
        self.players[player] = {
            'role': utils.get_new_role(self.players),
            'ready': False
        }
        self.client.send_message(
            "gasout.lobby",
            "player_quantity",
            join=str(len(self.players)),
            ready=str(utils.count_ready(self.players)),
            number=str(self.number)
        )

    def on_player_ready(self, player, msg, **kwargs):
        print('[READY]', player, msg)
        if self.game_running:
            print('[ERR]', 'game is running')
            return
        if player not in self.players:
            print('[ERR]', 'non-existed player')
            return
        
        self.players[player]['ready'] = (msg == '1')
        print(self.players)
        self.client.send_message(
            "gasout.lobby",
            "player_quantity",
            join=str(len(self.players)),
            ready=str(utils.count_ready(self.players)),
            number=str(self.number)
        )
        if utils.count_ready(self.players) >= 4:
            self.players = utils.random_role(self.players)
            for player in self.players:
                self.client.send_message(
                    player,
                    "game_start",
                    role=self.players[player]['role']
                )
            self.start()
    
    def on_player_leave(self, player, **kwargs):
        print('[LEAVE]', player)
        del self.players[player]
        if self.game_running:
            self.game_running = False
            self.client.send_message("gasout.forceclose", True)
        else:
            self.client.send_message(
                "gasout.lobby",
                "player_quantity",
                join=str(len(self.players)),
                ready=str(utils.count_ready(self.players)),
                number=str(self.number)
            )
        

    def on_message_gameover(self, **kwargs):
        print("[GAMEOVER]")
        self.game_running = False

    def on_message_truck(self, **kwargs):
        if self.solved: return
        print("[TRUCK]", kwargs)
        if kwargs.get("msg") == "decrease":
            self.score -= 10
        if self.score < 0:
            self.score = 0
        self.solve_value = None
        self.solved = True
        self.wrong = False
        print("score >>", self.score)

    def on_message_solve(self, **kwargs):
        if self.solved: return
        print("[SOLVE]", kwargs)
        self.solve_value = kwargs

    def send_actions_message(self, datas):
        if datas:  # if exist
            for data in datas:
                self.client.send_message(
                    data["topic"],
                    data["kwargs"]["msg"],
                    **{k: v for k, v in data["kwargs"].items() if k != "msg"},
                )

    def run(self):
        self.game_running = True
        time.sleep(2)
        self.client.send_message("gasout.all", "คุณมีเวลา 20 วินาทีในการอ่าน manual book")
        time.sleep(20)
        self.client.send_message("gasout.all", "เตรียมตัว")
        p = problems()
        problem = {}
        self.score = 0
        self.wrong = False

        while self.game_running:
            if self.solved:
                answers = []
                # reset exist action
                self.send_actions_message(problem.get("done_actions"))
                self.client.send_message("gasout.score", self.score)
                time.sleep(4)
                for i in [*range(3, 0, -1), "เริ่ม!!", ""]:
                    self.client.send_message("gasout.all", str(i))
                    time.sleep(1)
                
                # init problem
                if len(p) == 0:
                    p = problems()
                index = random.randrange(len(p))
                problem = p.pop(index)
                time_start = datetime.datetime.now()
                print("solutions >>", problem["solutions"])
                problem_receiver = utils.roles[
                    random.choice(problem.get("players")) - 1
                ]
                print("receiver >>", problem_receiver)

                # set action
                self.send_actions_message(problem.get("actions"))
                # send to quest to player
                self.client.send_message(problem_receiver, "quest", content=problem.get("name"))
                # start timer
                self.client.send_message("gasout.all", "timer", time=problem.get("time"))
                self.solved = False

            # TODO moved to SOLVED event
            if self.solve_value:
                item = self.solve_value['msg']
                solutions = problem['solutions']
                if item in solutions:
                    # it is the answer
                    print("Added >>", item)
                    # Added to answer list
                    if not (item in answers):
                        answers.append(item)
                    print("Answer >>", answers)

                    if set(answers) == set(solutions): 
                        # the answer is correct
                        status = 'ช้า!!'
                        used_time = (datetime.datetime.now() - time_start).seconds
                        if used_time <= 0.3 * problem['time'] and (not self.wrong):
                            self.score += 20
                            status = 'ยอดเยี่ยมที่สุด'
                        elif used_time <= 0.8 * problem['time']:
                            self.score += 10
                            status = 'ดีมาก'
                        print("solved! >>", status)

                        self.client.send_message("gasout.all", "stop_timer")
                        self.client.send_message("gasout.truck", "increase")
                        
                else:
                    # Wrong Items
                    # if problem["type"] == "passcode":
                    if item not in [
                        "gear-1",
                        "gear-2",
                        "gear-3",
                        "gear-4",
                        "gear-5",
                    ]:
                        self.client.send_message("gasout.all", "wrong", decrease_time=1)
                        self.wrong = True
                        print("wrong")

                # exit
                self.solve_value = None

            # tick
            time.sleep(0.01)

        # reset exist action
        self.send_actions_message(problem.get("done_actions"))
        # show score or something
        time.sleep(2)
        self.client.send_message("gasout.all", "************* END *************")
        print("End")
        self.client.change_topic(self.topic)
        self.client.send_message("gasout.close", True, number=self.number)
