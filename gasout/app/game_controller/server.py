import random
import argparse
from gasout.app.mqtt.client import MQTTClient
from gasout.app.game_controller.controller import GasOutController


class GasOutServer:

    def __init__(self, topic):
        self.topic = topic
        self.rooms = dict()
        self.scoreboard = [('player-1', 40), ('player-2', 20), ('player-3', 30), ("player-4", 50), ("player-5", 70)]
        self.client = MQTTClient(topic=topic)
        self.client.set_new_callback('gasout.host', self.host_room)
        self.client.set_new_callback('gasout.join', self.join_room)
        self.client.set_new_callback('gasout.close', self.on_room_closed)
        self.client.set_new_callback('gasout.score', self.on_score)
    
    def host_room(self, player, room_name, **kwargs):
        while (number := random.randint(1000, 9999)) in self.rooms:
            pass
        print('[HOST] create >>', f'{room_name}:{number}')
        self.rooms[int(number)] = GasOutController(topic=self.topic, name=room_name, number=number)
        self.client.send_message(player, True, number=number)
    
    def join_room(self, player, number, **kwargs):
        print('[JOIN]')
        if int(number) not in self.rooms:
            print('JOIN:ERR - room not existed')
            return
        
        room: GasOutController = self.rooms[int(number)]
        if room.game_running:
            print('JOIN:ERR - game is running')
            return
        if player in room.players:
            print('JOIN:ERR - seem like someone has a doppelganger')
            return
        if len(room.players) >= 4:
            print('JOIN:ERR - room full')
            return
        
        self.client.send_message(player, True, number=number)

    def on_room_closed(self, number, **kwargs):
        room = self.rooms[int(number)]
        room.client.loop_stop()
        room.game_running = False
        room.join()
        self.scoreboard.append((room.name, room.score))
        print(self.scoreboard)
        del self.rooms[int(number)]
    
    def on_score(self, **kwargs):
        print('[SCORE]')
        self.client.send_message('gasout.scoreboard', True, scores=self.scoreboard)

    def start(self):
        try:
            self.client.loop_forever()
        except KeyboardInterrupt:
            for room in self.rooms.values():
                room.client.loop_stop()
                room.game_running = False
                room.join()
            self.client.loop_stop()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--topic', help='topic of communication client')
    if not (topic := parser.parse_args().topic):
        topic = 'TEST-MQTT'
    print(topic)
    GasOutServer(topic=topic).start()
