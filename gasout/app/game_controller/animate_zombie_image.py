from kivy.uix.image import Image
from kivy.properties import StringProperty, NumericProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.clock import Clock


class AnimateZombiesImage(Image):
    time = 0.0
    rate = 0.1
    frame = 1

    def update(self, dt):
        # making truck move;
        self.time += dt
        if self.time > self.rate:
            self.time -= self.rate
            self.source = (
                "atlas://app/static/images/home/zombies/Zombies-"
                + str(self.frame)
            )
            # print(self.ids.truckatlas.source)
            self.frame = self.frame + 1
            if self.frame > 5:
                self.frame = 1
