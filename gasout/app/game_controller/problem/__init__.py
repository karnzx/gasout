import random


def random_number():
    words_number = [
        "ศูนย์",
        "หนึ่ง",
        "สอง",
        "สาม",
        "สี่",
        "ห้า",
        "หก",
        "เจ็ด",
        "แปด",
        "เก้า",
    ]
    number = '%04d' % random.randrange(9999)
    word = ' '.join([words_number[int(n)] for n in number])
    return number, word


players = [
    "gasout.first_player",
    "gasout.second_player",
    "gasout.third_player",
    "gasout.fourth_player",
]


# ---- Problems ---- #

def passcode():
    return {
        # Passcode
        "type": "passcode",
        "name": f">ใส่รหัสผ่าน: {(r := random_number())[1]} เดี๋ยวนี้!",
        "time": 10,
        "players": [2, 3, 4],
        "solutions": [r[0]],
    }
    
def passcode_pedal():
    return     {
        # 12
        "type": "fourth_player_button",
        "name": f"เกียร์ล็อกมีปัญหา เหยียบคันเร่งและ \n ใส่รหัสผ่าน: {(r := random_number())[1]} ",
        "time": 15,
        "players": [3, 4],
        "solutions": [r[0] , "pedal"],
    }

categories = [
    {
        # 3
        "type": "two_player_button",
        "name": "เครื่องยนต์ดับ!! `>ต้องทำอะไรสักอย่างแล้ววว!",
        "time": 18,
        "players": [4],
        "solutions": ["oil_can", "coolant", "gear-5", "pedal"],
        "actions": [
            {
                "topic": "gasout.third_player",
                "kwargs": {"msg": "engine_stop", "status": "stop"},
            },
            {
                "topic": "gasout.second_player",
                "kwargs": {"msg": "gear", "No.": random.randint(1, 3)},
            },
        ],
        "done_actions": [
            {
                "topic": "gasout.third_player",
                "kwargs": {"msg": "engine_stop", "status": "normal"},
            }
        ],
    },
    {
        # 4
        "type": "two_player_button",
        "name": "เชื้อเพลิงของพวกเรากำลังจะหมดลง! \n>ต้องรีบเติมน้ำมัน",
        "time": 16,
        "players": [1],
        "solutions": ["oil_can", "oil_tank"],
        "actions": [
            {
                "topic": "gasout.first_player",
                "kwargs": {"msg": "gas_gauge", "status": "decrease"},
            }
        ],
        "done_actions": [
            {
                "topic": "gasout.first_player",
                "kwargs": {"msg": "gas_gauge", "status": "increase"},
            }
        ],
    },
    {
        # 5
        "type": "three_player_button",
        "name": "เครื่องยนต์กำลังร้อนขึ้น! > ต้องทำให้เย็นลง \n:เปิดคู่มือ manal book ดูสิ",
        "time": 30,
        "players": [3],
        "solutions": ["omega", "coolant", "gear-1", "pedal"],
        "actions": [
            {
                "topic": "gasout.third_player",
                "kwargs": {"msg": "engine_heat", "status": "heat"},
            },
            {
                "topic": "gasout.second_player",
                "kwargs": {"msg": "gear", "No.": random.randint(2, 4)},
            },
        ],
        "done_actions": [
            {
                "topic": "gasout.third_player",
                "kwargs": {"msg": "engine_heat", "status": "normal"},
            }
        ],
    },
    {
        # 6
        "type": "two_player_button",
        "name": "มีอุโมงค์ข้างหน้า! \n เราต้องการแสงไฟเดี๋ยวนี้!",
        "time": 30,
        "players": [2, 4],
        "solutions": ["lever-on", "switch-on"],
        "actions": [
            {
                "topic": "gasout.third_player",
                "kwargs": {"msg": "switch", "state": "off"},
            },
            {
                "topic": "gasout.first_player",
                "kwargs": {"msg": "shake", "state": "start"},
            },
        ],
        "done_actions": [
            {
                "topic": "gasout.first_player",
                "kwargs": {"msg": "shake", "state": "stop"},
            },
        ],
    },
    {
        # 7
        "type": "two_player_button",
        "name": "โอเมก้าของเรากำลังจะหมด \n>เติมถังโอเมก้า",
        "time": 16,
        "players": [1 , 4],
        "solutions": ["omega", "coolant"],
    },
    {
        # 8
        "type": "fourth_player_button",
        "name": "แย่แล้ว! ล้อรถเราแตก \n พวกเราต้องเปลี่ยนยางรถ ",
        "time": 20,
        "players": [1, 2, 3],
        "solutions": ["hex-wrench", "tri-wrench", "wheel", "square-wrench", "circle-wrench", "tire"],
    },
    {
        # 9
        "type": "two_player_button",
        "name": "ถนนข้างหน้าขรุขระ \n เราต้องเปลี่ยนเกียร์ในการขับเคลื่อน",
        "time": 20,
        "players": [4],
        "solutions":["gear-2", "gear-3", "pedal"],
    },
    {
        # 10
        "type": "fourth_player_button",
        "name": "ถังน้ำมันรั่ว \n ต้องซ่อมถังและเติมน้ำมันอีกรอบ!!",
        "time": 20,
        "players": [1, 2, 3],
        "solutions": ["hex-wrench", "oil_can", "tri-wrench", "oil_tank", "square-wrench", "circle-wrench"],
    },
    {
        # 11
        "type": "fourth_player_button",
        "name": "เครื่องยนต์พัง!! ต้องซ่อมด้วยประแจ \n และเหยียบคันเร่งด้วยเกียร์ 1 ",
        "time": 20,
        "players": [2, 3, 4],
        "solutions": ["hex-wrench", "tri-wrench", "square-wrench", "circle-wrench", "gear-1",  "pedal"],
    },
]


# ---- Used Problem ---- #
def problems():
    return [
        passcode(),
        categories[0],
        categories[1],
        categories[2],
        categories[3],
        categories[4],
        categories[5],
        categories[6],
        categories[7],
        categories[8],
        passcode_pedal()
    ]

