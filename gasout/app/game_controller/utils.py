import random

roles = [
    "gasout.first_player",
    "gasout.second_player",
    "gasout.third_player",
    "gasout.fourth_player",
]


def get_new_role(players):
    remains = roles[:]
    for player in players.values():
        if player['role'] in remains:
            remains.remove(player['role'])
    return remains[0]

def random_role(players):
    remains = roles[:]
    for player in players:
        players[player]['role'] = remains.pop(random.randrange(0, len(remains)))
    return players

def count_ready(players):
    return sum([1 for player in players.values() if player.get('ready', False)])
